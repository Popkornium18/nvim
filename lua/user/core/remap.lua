local utils = require("user.core.utils")

local function toggle_ansible_vault()
  local first_line = vim.api.nvim_buf_get_lines(0, 0, 1, false)[1]
  if string.match(first_line, "^$ANSIBLE") then
    vim.cmd("%! ansible-vault decrypt")
  else
    vim.cmd("%! ansible-vault encrypt")
  end
end

-- TODO: Remove register maps
utils.register_maps({
  -- Better Visual Block indenting
  { "v", ">", ">gv", { desc = "Visual indent right" } },
  { "v", "<", "<gv", { desc = "Visual indent left" } },
  -- Keep the cursor centered
  { "n", "J", "mzJ`z", { desc = "Keep cursor in place when joining lines" } },
  { "n", "<C-d>", "<C-d>zz", { desc = "Jump down: Keep cursor centered" } },
  { "n", "<C-u>", "<C-u>zz", { desc = "Jump up: Keep cursor centered" } },
  { "n", "n", "nzzzv", { desc = "Search down: Keep cursor centered" } },
  { "n", "N", "Nzzzv", { desc = "Search up: Keep cursor centered" } },
  -- Better tab switching
  { "n", "<A-h>", ":tabprevious<CR>", { desc = "Tab navigation left" } },
  { "n", "<A-l>", ":tabnext<CR>", { desc = "Tab navigation right" } },
  -- Misc maps
  { "n", "<leader>w", ":set invwrap<CR>", { desc = "Toggle line wrapping" } },
  { "n", "<leader>n", utils.toggle_clutter, { desc = "Toggle copy mode" } },
  { "x", "<leader>p", '"_dP', { desc = "Paste, but keep copy register" } },
  { "n", "av", toggle_ansible_vault, { desc = "Toggle ansible-vault encryption" } },
})
