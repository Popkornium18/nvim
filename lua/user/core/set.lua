-- Visuals
vim.opt.cmdheight = 0
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.hlsearch = false
vim.opt.wrap = false
vim.opt.scrolloff = 8
vim.opt.termguicolors = true
vim.opt.laststatus = 3
vim.opt.list = true

-- Functionality
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.autoindent = true -- Needed for Treesitter indentexpr
vim.opt.hidden = true
vim.opt.mouse = "a"
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.swapfile = false
vim.opt.undodir = os.getenv("HOME") .. "/.local/share/nvim/undodir"
vim.opt.undofile = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.updatetime = 50

vim.g.mapleader = " "

-- Device-specific stuff
local utils = require("user.core.utils")
if utils.device_is_workstation() then
  vim.opt.clipboard = "unnamedplus" -- Share clipboard with OS
  vim.opt.indentexpr = "v:lua.vim.treesitter.indent()"
  vim.opt.foldmethod = "expr"
  vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
  vim.opt.foldlevelstart = 1
  vim.opt.foldnestmax = 4
  vim.opt.foldtext = ""
  vim.opt.foldminlines = 4
end
