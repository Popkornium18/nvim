local M = {}
-- Usage: { mode, lhs, rhs, { option1 = value1 } },
M.register_maps = function(maps)
  for _, map in pairs(maps) do
    M.register_map(map)
  end
end

M.register_map = function(map)
  local modes = map[1]

  for mode in modes:gmatch(".") do
    local rhs = map[3]
    local lhs = map[2]
    local options = map[4] or {}
    local opts = {
      remap = false,
      silent = true,
    }
    local filetype = nil

    for key, value in pairs(options) do
      if key == "unmap" then
        if value then
          value = rhs
          vim.api.nvim_set_keymap(mode, value, "", {})
        end
      elseif key == "insert_to_normal" then
        if mode == "i" then
          if value == true then
            rhs = "<esc>" .. rhs .. "a"
          else
            rhs = value
          end
        end
      elseif key == "filetype" then
        filetype = value
        opts.buffer = true
        break
      else
        opts[key] = value
      end
    end

    local keymap_set = function()
      vim.keymap.set(mode, lhs, rhs, opts)
    end
    if filetype then
      M.register_autocmd({ "FileType", keymap_set, { pattern = filetype } })
    else
      keymap_set()
    end
  end
end

-- Usage: { event, callaback, { option1 = value1 } },
M.register_autocmds = function(autocmds)
  for _, autocmd in ipairs(autocmds) do
    M.register_autocmd(autocmd)
  end
end

M.register_autocmd = function(autocmd)
  local event = autocmd[1]
  local callback = autocmd[2]
  local options = autocmd[3] or {}
  options = vim.tbl_extend("error", options, {
    callback = callback,
  })

  if options.buffer == true then
    options.buffer = 0
  end

  vim.api.nvim_create_autocmd({ event }, options)
end

M.device_is_workstation = function()
  return os.getenv("HOST_TYPE") == "workstations"
end

M.toggle_clutter = function()
  local on = vim.o.signcolumn == "auto"

  vim.o.signcolumn = on and "no" or "auto"
  vim.o.cursorcolumn = not vim.o.cursorcolumn
  vim.o.cursorline = not vim.o.cursorline
  vim.o.number = not vim.o.number
  vim.o.relativenumber = not vim.o.relativenumber
  vim.o.list = not vim.o.list

  vim.cmd("IBLToggle")
  require("scrollbar.utils").toggle()

  if on then
    vim.diagnostic.hide()
  else
    vim.diagnostic.show()
  end

  if M.device_is_workstation() then
    vim.cmd("GitBlameToggle")
  end
end

---@param text string
---@return table<string>
---Split a string on newline characters
M.split_lines = function(text)
  local output = {} ---@type table<string>
  for line in string.gmatch(text, "[^\n\r]+") do
    table.insert(output, line)
  end
  return output
end

---@param command string
---@return boolean? success
---@return table<string> stdout
---@return table<string> stderr
---Run a command anc capture stdout and stderr
M.execute_command = function(command)
  assert(not string.find(command, ">"))
  local stdout_filename = os.tmpname()
  local stderr_filename = os.tmpname()
  local exit = os.execute(command .. " > " .. stdout_filename .. " 2> " .. stderr_filename)

  local stdout_file = io.open(stdout_filename)
  assert(stdout_file)
  local stdout = stdout_file:read("*all") ---@type string
  stdout_file:close()

  local stderr_file = io.open(stderr_filename)
  assert(stderr_file)
  local stderr = stderr_file:read("*all") ---@type string
  stderr_file:close()

  return exit, M.split_lines(stdout), M.split_lines(stderr)
end

return M
