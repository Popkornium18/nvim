return {
  "nvim-lualine/lualine.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  config = function()
    local lualine = require("lualine")
    local lazy_status = require("lazy.status") -- to configure lazy pending updates count

    local branch = {
      "branch",
      icon = "",
    }

    local diff = {
      "diff",
      symbols = { added = " ", modified = " ", removed = " " },
    }

    local diagnostics = {
      "diagnostics",
      sources = { "nvim_diagnostic" },
      sections = { "error", "warn", "hint", "info" },
      symbols = { error = " ", warn = " ", hint = " ", info = " " },
      always_visible = false,
    }

    local filetype = {
      "filetype",
      icon_only = true,
    }

    local filename = {
      "filename",
      icon_only = true,
      file_status = true,
      symbols = {
        modified = " ● ",
        readonly = "  ",
        unnamed = "",
      },
    }

    local function show_macro_recording()
      local recording_register = vim.fn.reg_recording()
      if recording_register == "" then
        return ""
      else
        return "Recording @" .. recording_register
      end
    end

    local sections = {
      lualine_a = { "mode" },
      lualine_b = {
        branch,
        diff,
        {
          "macro-recording",
          fmt = show_macro_recording,
        },
      },
      lualine_c = { diagnostics },
      lualine_x = { { lazy_status.updates, cond = lazy_status.has_updates } },
      lualine_y = { filetype, filename },
      lualine_z = { "location", "progress" },
    }

    -- configure lualine with modified theme
    lualine.setup({
      options = {
        icons_enabled = true,
        component_separators = { left = "", right = "" },
        section_separators = { left = "", right = "" },
        always_divide_middle = false,
        gloabalstatus = true,
        colored = true,
        update_in_insert = true,
        theme = "auto",
      },
      sections = sections,
    })
  end,
}
