return {
  {
    "neovim/nvim-lspconfig",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "b0o/SchemaStore.nvim",
    },
    config = function()
      -- import lspconfig plugin
      local lspconfig = require("lspconfig")

      -- import cmp-nvim-lsp plugin
      local cmp_nvim_lsp = require("cmp_nvim_lsp")

      local capabilities = cmp_nvim_lsp.default_capabilities()

      -- Change the Diagnostic symbols in the sign column (gutter)
      local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
      for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
      end

      vim.diagnostic.config({
        virtual_text = true,
        signs = {
          active = signs,
        },
        update_in_insert = true,
        underline = true,
        severity_sort = true,
        float = {
          focusable = false,
          style = "minimal",
          border = "rounded",
          source = "if_many",
          header = "",
          prefix = "",
        },
      })

      local keymap = vim.keymap
      local opts = { noremap = true, silent = true }
      opts.desc = "LSP: Go to previous diagnostic"
      keymap.set("n", "gE", vim.diagnostic.goto_prev, opts)

      opts.desc = "LSP: Go to next diagnostic"
      keymap.set("n", "ge", vim.diagnostic.goto_next, opts)

      -- Use LspAttach autocommand to only map the following keys
      -- after the language server attaches to the current buffer
      vim.api.nvim_create_autocmd("LspAttach", {
        group = vim.api.nvim_create_augroup("UserLspConfig", {}),
        callback = function(ev)
          opts.buffer = ev.buf
          opts.desc = "LSP: Show references"
          keymap.set("n", "gr", function()
            require("telescope.builtin").lsp_references(require("telescope.themes").get_ivy())
          end, opts)

          opts.desc = "LSP: Show incoming calls"
          keymap.set("n", "gi", function()
            require("telescope.builtin").lsp_incoming_calls(require("telescope.themes").get_ivy())
          end, opts)

          opts.desc = "LSP: Show outgoing calls"
          keymap.set("n", "go", function()
            require("telescope.builtin").lsp_outgoing_calls(require("telescope.themes").get_ivy())
          end, opts)

          opts.desc = "LSP: Jump to type definition of symbol"
          keymap.set("n", "gt", function()
            require("telescope.builtin").lsp_type_definitions(require("telescope.themes").get_ivy())
          end, opts)

          opts.desc = "LSP: Jump to definition of symbol"
          keymap.set("n", "gd", function()
            require("telescope.builtin").lsp_definitions(require("telescope.themes").get_ivy())
          end, opts)

          opts.desc = "LSP: Show implementations"
          keymap.set("n", "gI", function()
            require("telescope.builtin").lsp_implementations(require("telescope.themes").get_ivy())
          end, opts)

          opts.desc = "LSP: Code actions"
          keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts)

          opts.desc = "LSP: Rename symbol"
          keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)

          opts.desc = "LSP: Show buffer diagnostics"
          keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics bufnr=0<CR>", opts)

          opts.desc = "LSP: Show line diagnostics"
          keymap.set("n", "<leader>d", vim.diagnostic.open_float, opts)

          opts.desc = "LSP: Display hover information"
          keymap.set("n", "K", vim.lsp.buf.hover, opts)

          opts.desc = "LSP: Restart LSP"
          keymap.set("n", "<leader>rs", ":LspRestart<CR>", opts)

          opts.desc = "LSP: Toggle Inlay Hints"
          keymap.set("n", "<leader>ih", function()
            vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled(nil))
          end, opts)
        end,
      })

      local lsp_servers_basic_setup = {
        "ansiblels",
        "awk_ls",
        "basedpyright",
        "bashls",
        "clangd",
        "cmake",
        "cssls",
        "dockerls",
        "helm_ls",
        "jsonls",
        "yamlls",
      }
      for _, lsp in ipairs(lsp_servers_basic_setup) do
        lspconfig[lsp].setup({ capabilities = capabilities })
      end

      lspconfig.jsonls.setup({
        settings = {
          json = {
            schemas = require("schemastore").json.schemas(),
            validate = { enable = true },
          },
        },
      })

      lspconfig.yamlls.setup({
        settings = {
          yaml = {
            schemaStore = {
              -- You must disable built-in schemaStore support if you want to use
              -- this plugin and its advanced options like `ignore`.
              enable = false,
              -- Avoid TypeError: Cannot read properties of undefined (reading 'length')
              url = "",
            },
            schemas = require("schemastore").yaml.schemas(),
          },
        },
      })

      lspconfig.html.setup({
        filetypes = { "html", "htmldjango" },
      })

      lspconfig.ts_ls.setup({
        capabilities = capabilities,
        single_file_support = true,
        completions = {
          completeFunctionCalls = true,
        },

        settings = {
          javascript = {
            inlayHints = {
              includeInlayEnumMemberValueHints = true,
              includeInlayFunctionLikeReturnTypeHints = true,
              includeInlayFunctionParameterTypeHints = true,
              includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
              includeInlayParameterNameHintsWhenArgumentMatchesName = true,
              includeInlayPropertyDeclarationTypeHints = true,
              includeInlayVariableTypeHints = false,
            },
          },

          typescript = {
            inlayHints = {
              includeInlayEnumMemberValueHints = true,
              includeInlayFunctionLikeReturnTypeHints = true,
              includeInlayFunctionParameterTypeHints = true,
              includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
              includeInlayParameterNameHintsWhenArgumentMatchesName = true,
              includeInlayPropertyDeclarationTypeHints = true,
              includeInlayVariableTypeHints = false,
            },
          },
        },
      })

      -- configure lua server (with special settings)
      lspconfig.lua_ls.setup({
        capabilities = capabilities,
        settings = { -- custom settings for lua
          Lua = {
            -- make the language server recognize "vim" global
            diagnostics = {
              globals = { "vim" },
            },
            workspace = {
              -- make language server aware of runtime files
              library = {
                [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                [vim.fn.stdpath("config") .. "/lua"] = true,
              },
            },
          },
        },
      })
      lspconfig.dockerls.setup({
        settings = {
          docker = {
            languageserver = {
              formatter = {
                ignoreMultilineInstructions = true,
              },
            },
          },
        },
      })
    end,
  },
  {
    "antosha417/nvim-lsp-file-operations",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-tree.lua",
    },
    config = function()
      require("lsp-file-operations").setup()
    end,
  },
}
