---@param repo string
---@return table
local function read_args_from_pre_commit(repo)
  local utils = require("user.core.utils")
  local args = {} ---@type table<string>
  local gitroot_success, gitroot_stdout, _ = utils.execute_command("git rev-parse --show-toplevel")

  if gitroot_success then
    local file = gitroot_stdout[1] .. "/.pre-commit-config.yaml"
    local yq_cmd = "yq '.repos[] | select(.repo == \"" .. repo .. "\").hooks[0].args[]' < " .. file
    local yq_success, yq_stdout, _ = utils.execute_command(yq_cmd)
    if yq_success then
      for _, value in ipairs(yq_stdout) do
        if string.find(value, " ") then
          value = '"' .. value .. '"'
        end
        table.insert(args, value)
      end
    end
  end

  return args
end

local python_major_minor_cmd = "python3 -c \"import sys;i=sys.version_info;print(f'{i.major}{i.minor}')\""

return {
  "mhartington/formatter.nvim",
  event = { "BufWritePre", "FileWritePre", "FileAppendPre" },
  config = function()
    -- Utilities for creating configurations
    local fmt_util = require("formatter.util")

    -- Provides the Format, FormatWrite, FormatLock, and FormatWriteLock commands
    require("formatter").setup({
      -- Enable or disable logging
      logging = true,
      -- Set the log level
      log_level = vim.log.levels.WARN,
      -- All formatter configurations are opt-in
      filetype = {
        -- Formatter configurations for filetype "lua" go here
        -- and will be executed in order
        lua = {
          require("formatter.filetypes.lua").stylua,
          function()
            return {
              exe = "stylua",
              args = {
                "--search-parent-directories",
                "--indent-type=Spaces",
                "--indent-width=" .. tostring(vim.o.tabstop),
                "--stdin-filepath",
                fmt_util.escape_path(fmt_util.get_current_buffer_file_path()),
                "--",
                "-",
              },
              stdin = true,
            }
          end,
        },

        sh = {
          require("formatter.filetypes.sh").shfmt,
          function()
            return {
              exe = "shfmt",
              args = {
                "--indent=" .. tostring(vim.o.tabstop),
                "--space-redirects",
                "--case-indent",
                "--func-next-line",
                fmt_util.escape_path(fmt_util.get_current_buffer_file_path()),
              },
              stdin = true,
            }
          end,
        },

        dockerfile = { vim.lsp.buf.format },

        python = {
          function()
            local utils = require("user.core.utils")
            local args = read_args_from_pre_commit("https://github.com/asottile/pyupgrade")

            if next(args) == nil then
              local _, major_minor, _ = utils.execute_command(python_major_minor_cmd)
              args = { "--exit-zero-even-if-changed", "--py" .. major_minor[1] .. "-plus" }
            else
              args = { "--exit-zero-even-if-changed", unpack(args) }
            end

            return {
              exe = "pyupgrade",
              args = args,
              stdin = false,
            }
          end,

          function()
            local utils = require("user.core.utils")
            local args = read_args_from_pre_commit("https://github.com/asottile/reorder_python_imports")

            if next(args) == nil then
              local _, major_minor, _ = utils.execute_command(python_major_minor_cmd)
              args = {
                "-",
                "--exit-zero-even-if-changed",
                "--py" .. major_minor[1] .. "-plus",
                "--add-import",
                "'from __future__ import annotations'",
              }
            else
              args = { "-", "--exit-zero-even-if-changed", unpack(args) }
            end
            return {
              exe = "reorder-python-imports",
              args = args,
              stdin = true,
            }
          end,

          require("formatter.filetypes.python").black,
        },

        awk = { require("formatter.filetypes.awk").prettier },
        css = { require("formatter.filetypes.css").prettier },
        graphql = { require("formatter.filetypes.graphql").prettier },
        html = { require("formatter.filetypes.html").prettier },
        htmldjango = { require("formatter.filetypes.html").prettier },
        javascript = { require("formatter.filetypes.javascript").prettier },
        json = { require("formatter.filetypes.json").prettier },
        latex = { require("formatter.filetypes.latex").latexindent },
        tex = { require("formatter.filetypes.tex").latexindent },
        typescript = { require("formatter.filetypes.typescript").prettier },
        yaml = { require("formatter.filetypes.yaml").prettier },

        -- Use the special "*" filetype for defining formatter configurations on
        -- any filetype
        ["*"] = {
          -- "formatter.filetypes.any" defines default configurations for any
          -- filetype
          require("formatter.filetypes.any").remove_trailing_whitespace,
        },
      },
    })

    local auto_fmt_grp = vim.api.nvim_create_augroup("AutoFmtGrp", { clear = true })
    vim.api.nvim_create_autocmd("BufWritePost", { command = "FormatWrite", group = auto_fmt_grp })
  end,
}
