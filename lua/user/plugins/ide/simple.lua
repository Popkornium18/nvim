return {
  "nvim-lua/plenary.nvim",
  { "mfussenegger/nvim-ansible", event = { "BufReadPre", "BufNewFile" }, ft = { "yaml" } },
  { "towolf/vim-helm", event = { "BufReadPre", "BufNewFile" }, ft = { "yaml" } },
  { "folke/neodev.nvim", event = { "BufReadPre", "BufNewFile" }, ft = { "lua" }, config = true },
  { "RRethy/vim-illuminate", event = { "BufReadPre", "BufNewFile" } },
  { "lervag/vimtex", event = { "BufReadPre", "BufNewFile" }, ft = { "tex", "plaintex" } },
  { "lewis6991/gitsigns.nvim", event = { "BufReadPre", "BufNewFile" }, config = true },
  { "f-person/git-blame.nvim", event = { "BufReadPre", "BufNewFile" }, config = true },
}
