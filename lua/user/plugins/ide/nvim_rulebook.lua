return {
  "chrisgrieser/nvim-rulebook",
  keys = {
    {
      "<leader>ri",
      function()
        require("rulebook").ignoreRule()
      end,
      "n",
      desc = "Ignore linter rule",
    },
    {
      "<leader>rl",
      function()
        require("rulebook").lookupRule()
      end,
      "n",
      desc = "Look-up linter rule",
    },
  },
}
