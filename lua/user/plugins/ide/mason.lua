return {
  "williamboman/mason.nvim",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "williamboman/mason-lspconfig.nvim",
    "WhoIsSethDaniel/mason-tool-installer.nvim",
    "mhartington/formatter.nvim",
    "mfussenegger/nvim-lint",
    "neovim/nvim-lspconfig",
  },
  keys = {
    {
      "<leader>li",
      ":Mason<cr>",
      "n",
      desc = "Open Mason UI",
    },
  },
  config = function()
    local mason = require("mason")
    local mason_lspconfig = require("mason-lspconfig")
    local mason_tool_installer = require("mason-tool-installer")

    -- enable mason and configure icons
    mason.setup({
      ui = {
        icons = {
          package_installed = "✓",
          package_pending = "➜",
          package_uninstalled = "✗",
        },
      },
    })

    mason_lspconfig.setup({
      ensure_installed = {
        "ansiblels",
        "awk_ls",
        "basedpyright",
        "bashls",
        "clangd",
        "cmake",
        "cssls",
        "dockerls",
        "html",
        "jsonls",
        "lua_ls",
        "ts_ls",
        "yamlls",
      },
      automatic_installation = true,
      auto_update = true,
    })

    mason_tool_installer.setup({
      -- a list of all tools you want to ensure are installed upon
      -- start; they should be the names Mason uses for each tool
      ensure_installed = {
        "ansible-lint",
        "black",
        "latexindent",
        "bash-debug-adapter",
        "debugpy",
        "prettier",
        "reorder-python-imports",
        "shellcheck",
        "shfmt",
        "stylua",
      },
      run_on_start = true,
      auto_update = true,
    })
  end,
}
