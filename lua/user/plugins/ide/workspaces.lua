return {
  "natecraddock/workspaces.nvim",
  event = { "VeryLazy" },

  config = function()
    local telescope = require("telescope.builtin")
    require("workspaces").setup({
      hooks = {
        open = {
          function()
            telescope.git_files()
          end,
        },
      },
    })
  end,
}
