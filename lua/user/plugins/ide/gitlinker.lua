return {
  {
    "linrongbin16/gitlinker.nvim",
    cmd = "GitLink",
    opts = {},
    keys = {
      { "<leader>gy", "<cmd>GitLink<cr>", mode = { "n", "v" }, desc = "Yank git link" },
      { "<leader>gY", "<cmd>GitLink!<cr>", mode = { "n", "v" }, desc = "Open git link" },
    },
    config = function()
      require("gitlinker").setup({
        router = {
          browse = { ["^gitlab%.local%.hacon%.de"] = require("gitlinker.routers").gitlab_browse },
          blame = { ["^gitlab%.local%.hacon%.de"] = require("gitlinker.routers").gitlab_blame },
        },
      })
    end,
  },
}
