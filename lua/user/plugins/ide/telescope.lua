return {
  {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = {
      "nvim-lua/plenary.nvim",
      {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build", -- luacheck: ignore 631
      },
      "nvim-tree/nvim-web-devicons",
    },

    keys = function(_, keys)
      local telescope = require("telescope")
      local builtin = require("telescope.builtin")
      local ws = telescope.extensions.workspaces

      local function pick_file_or_repo()
        local success = pcall(builtin.git_files)
        if not success then
          ws.workspaces()
        end
      end

      -- some additional telescope mappings are configured in the workspaces.nvim config
      local picker_mappings = {
        { "<leader>o", pick_file_or_repo, "n", desc = "TLS: Pick file in git repo" },
        { "<leader>O", builtin.find_files, "n", desc = "TLS: Pick file in the current dir" },
        { "<leader>tf", builtin.oldfiles, "n", desc = "TLS: Pick previously opened file" },
        { "<leader>tg", builtin.live_grep, "n", desc = "TLS: Pick file (grep contents)" },
        { "<leader>tb", builtin.buffers, "n", desc = "TLS: Pick buffer" },
        { "<leader>tm", builtin.keymaps, "n", desc = "TLS: List keymaps" },
        { "<leader>tv", builtin.vim_options, "n", desc = "TLS: Change nvim option" },
        { "<leader>tc", builtin.commands, "n", desc = "TLS: Pick and run command" },
        { "<leader>th", builtin.help_tags, "n", desc = "TLS: Open help" },
        { "<leader>tR", builtin.registers, "n", desc = "TLS: Pick and paste register" },
        { "<leader>tM", builtin.man_pages, "n", desc = "TLS: Pick man page" },
        { "<leader>t.", builtin.resume, "n", desc = "TLS: Resume previous picker" },
        { "<leader>tr", ws.workspaces, "n", desc = "TLS: Pick a workspace" },
      }
      return vim.list_extend(picker_mappings, keys)
    end,

    config = function()
      local telescope = require("telescope")
      local actions = require("telescope.actions")
      telescope.load_extension("fzf")

      local telescope_mappings = {
        ["<C-k>"] = actions.move_selection_previous,
        ["<C-j>"] = actions.move_selection_next,
        ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
        ["<C-s>"] = actions.select_horizontal,
      }
      telescope.setup({
        defaults = {
          path_display = { "truncate " },
          mappings = {
            i = telescope_mappings,
            n = telescope_mappings,
          },
        },
        pickers = {
          live_grep = {
            additional_args = function()
              return { "--hidden", "--glob=!.git" }
            end,
          },
        },
      })
    end,
  },
  {
    "nvim-telescope/telescope-ui-select.nvim",
    config = function()
      require("telescope").setup({
        extensions = {
          ["ui-select"] = {
            require("telescope.themes").get_dropdown({}),
          },
        },
      })
      require("telescope").load_extension("ui-select")
    end,
  },
}
