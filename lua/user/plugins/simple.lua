return {
  "nvim-tree/nvim-web-devicons",
  "christoomey/vim-tmux-navigator",
  "RyanMillerC/better-vim-tmux-resizer",
  {
    "MeanderingProgrammer/render-markdown.nvim",
    opts = {},
    dependencies = { "nvim-treesitter/nvim-treesitter", "nvim-tree/nvim-web-devicons" },
  },
  { "numToStr/Comment.nvim", event = { "BufReadPre", "BufNewFile" }, config = true },
  { "petertriho/nvim-scrollbar", event = { "BufReadPre", "BufNewFile" }, config = true },
}
